/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenu1;

/**
 *
 * @author Janis
 */
public class EmpleadoDocente extends Empleado{
    private int nivel;
    private float horasLaboradas;
    private float pagoHora;

    public EmpleadoDocente() {
        this.nivel=0;
        this.horasLaboradas=0.0f;
        this.pagoHora=0.0f;
    }

    public EmpleadoDocente(int nivel, float horasLab, float pagoHora, int numEmpleado, String nombre, String domicilio, Contrato contrato) {
        super(numEmpleado, nombre, domicilio, contrato);
        this.nivel = nivel;
        this.horasLaboradas = horasLab;
        this.pagoHora = pagoHora;
    }

    

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getHorasLab() {
        return horasLaboradas;
    }

    public void setHorasLab(float horasLab) {
        this.horasLaboradas = horasLab;
    }

    public float getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoHora = pagoHora;
    }
    
    @Override
    public float calcularTotal(int tipo) {
       float total=0;
       total=this.pagoHora*this.horasLaboradas;
       if(tipo==1)
           total=total+ (total*0.35f);
       if(tipo==2)
           total=total + (total*0.40f);
       if(tipo==3)
           total=total + (total*0.50f);
       else
           return total;
      return total;
    }

    @Override
    public float calcularImpuesto() {
        return (this.pagoHora*this.horasLaboradas) * (this.contrato.getImpuesto()/100);
    }
    
}
