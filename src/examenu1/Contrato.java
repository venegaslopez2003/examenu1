/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenu1;

/**
 *
 * @author Janis
 */
public class Contrato {
    private int clave;
    private String puesto;
    private float Impuesto;

    public Contrato() {
        this.clave=0;
        this.puesto="";
        this.Impuesto=0.0f;
    }

    public Contrato(int claveC, String puesto, float ImpuestoISR) {
        this.clave = claveC;
        this.puesto = puesto;
        this.Impuesto = ImpuestoISR;
    }

    public int getClaveC() {
        return clave;
    }

    public void setClaveC(int claveC) {
        this.clave = claveC;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public float getImpuesto() {
        return Impuesto;
    }

    public void setImpuesto(float Impuesto) {
        this.Impuesto = Impuesto;
    }
    
    
    
}
